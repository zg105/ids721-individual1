+++
title = "Week5 mini Project"
description = "Use Zola to develop a static site."
date = 2024-03-02T09:19:42+00:00
updated = 2024-03-02T09:19:42+00:00
draft = false
template = "portfolio/page.html"

[extra]
lead = "This is the readme of the <b>AWS Lambda Function</b> program."
+++

- Create a Rust AWS Lambda function
- Implement a simple service
- Connect to a databse (MySQL)

link: https://gitlab.oit.duke.edu/zg105/ids-week5-miniproj