+++
title = "Week3 mini Project"
description = "Use Zola to develop a static site."
date = 2024-02-16T09:19:42+00:00
updated = 2024-02-16T09:19:42+00:00
draft = false
template = "portfolio/page.html"

[extra]
lead = "This is the readme of the <b>AWS S3</b> program."
+++

In this project, I created an S3 Bucket using CDK with AWS CodeWhisperer. 
The `cdk.json` file tells the CDK Toolkit how to execute your app.

To start with, I need add CodeWhisper to vscode extension and log into my AWS account.

Then, run `npm install -g aws-cdk` to install aws cdk.

Then run `cdk init app --language=typescript` to init an app using python typescript.

Modified the app in `stack.ts` file with the help of CodeWhisper, which provided code prompt for us.
You can add the encryption and version configuration.

link: https://gitlab.oit.duke.edu/zg105/ids721-week3-miniproj
